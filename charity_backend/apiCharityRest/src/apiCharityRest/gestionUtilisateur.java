package apiCharityRest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import CharityLandEJB.entities.Admin;
import CharityLandEJB.entities.Association;
import CharityLandEJB.entities.Donateur;
import CharityLandEJB.entities.Utilisateur;
import CharityLandEJB.services.GestionUsersRemote;

@Path("/")
public class gestionUtilisateur {
	@EJB
	GestionUsersRemote gestionUsersRemote;
	Utilisateur user = new Utilisateur();
	List<Utilisateur> users = new ArrayList<Utilisateur>();

	@PostConstruct
	public void init() {
		users = gestionUsersRemote.findAllUsers();
		System.out.println(users);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/users")
	public Response getUsers() {
		return Response.status(Status.ACCEPTED).entity(this.users).header("Access-Control-Allow-Origin", "*").allow("OPTIONS").build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/users/{cid}")
	public Response getUserById(@PathParam("cid") int cid) {
			
		Utilisateur finduser=gestionUsersRemote.findUserByCid(cid);
		return Response.status(Status.ACCEPTED).entity(finduser).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/users")
	public Response addUser(Utilisateur util) {
		gestionUsersRemote.ajouterUser(util);
		return Response.status(Status.ACCEPTED).entity("user ajouter avec success !!").build();
	}
	@POST
	@Consumes({MediaType.APPLICATION_FORM_URLENCODED})
	@Produces({MediaType.APPLICATION_JSON})
	@Path(value="/users/login")
	public Response loginUser(@FormParam("mail") String mail, @FormParam("pass")String password) {
		Utilisateur u;
		u=gestionUsersRemote.LoginUser(mail, password);
		if(u==null) {
		   return Response.status(Status.ACCEPTED).entity("login failed  !!").build();
		   }
		else {
			return Response.status(Status.ACCEPTED).entity("login success !!").build();
			}
		
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users/{cid}")
	public Response updateUser(@PathParam("cid") int cid, Utilisateur util) {
		gestionUsersRemote.updateUser(util);
		return Response.status(Status.ACCEPTED).entity("modifier avec success !!").build();
	}

	@DELETE
	@Path("/users/{cid}")
	public Response deleteAdmin(@PathParam("cid") int cid) {
		Utilisateur util=gestionUsersRemote.findUserByCid(cid);
		gestionUsersRemote.deleteUser(util);
		return Response.status(Status.ACCEPTED).entity("supprimer avec success !!").build();
	}
	// ///////////////////////////////////////////////                   rest api donateur
	Donateur donateur = new Donateur();
	List<Donateur> don = new ArrayList<Donateur>();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/donateur")
	public List<Donateur> getDonateurs() {
		return gestionUsersRemote.findAllDonateur();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/donateur/{cid}")
	public Donateur getDonateurByCid(@PathParam("cid") int cid) {

		return gestionUsersRemote.findDonateurByCid(cid);
		// Response.status(Status.ACCEPTED).entity(gestion.AfficherUsers()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/donateur")
	public Response addDonateur(Donateur user) {
		gestionUsersRemote.ajouterDonateur(user);
		return Response.status(Status.ACCEPTED).entity("donateur ajouter avec success !!").build();
	}
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/donateur/login")
	public Response loginDonateur(@QueryParam("mail") String mail, @QueryParam("pass")String password) {
		Donateur u;
		u=gestionUsersRemote.LoginDonateur(mail, password);
		if(u==null) {
		   return Response.status(Status.ACCEPTED).entity("login failed  !!").build();
		   }
		else {
			return Response.status(Status.ACCEPTED).entity("login success !!").build();
			}
		
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/donateur/{cid}")
	public Response updateDonateur(@PathParam("cid") int cid) {
		Donateur util=gestionUsersRemote.findDonateurByCid(cid);
		gestionUsersRemote.updateDonateur(util);
		return Response.status(Status.ACCEPTED).entity("donateur modifier avec success !!").build();
	}

	@DELETE
	@Path("/donateur/{cid}")
	public void deleteDonateur(@PathParam("cid") int cid) {
		Donateur util=gestionUsersRemote.findDonateurByCid(cid);
		gestionUsersRemote.deleteDonateur(util);
	}
/////////////////////////////////////////////////////////////// rest api association
	Association association = new Association();
	List<Association> assoc = new ArrayList<Association>();

	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/association")
	public List<Association> getAssociations() {
		return gestionUsersRemote.findAllAssociation();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/association/{cid}")
	public Association getAssociationByCid(@PathParam("cid") int cid) {

		return gestionUsersRemote.findAssociationByCid(cid);
		// Response.status(Status.ACCEPTED).entity(gestion.AfficherUsers()).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/association")
	public Response addAssociation(Association association) {
		gestionUsersRemote.ajouterAssociation(association);
		return Response.status(Status.ACCEPTED).entity("Association ajouter avec success !!").build();
	}
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/association/login")
	public Response loginAssociation(@QueryParam("mail") String mail, @QueryParam("pass")String password) {
		Association u;
		u=gestionUsersRemote.LoginAssociation(mail, password);
		if(u==null) {
		   return Response.status(Status.ACCEPTED).entity("login failed  !!").build();
		   }
		else {
			return Response.status(Status.ACCEPTED).entity("login success !!").build();
			}
		
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/association/{cid}")
	public Response updateAssociation(@PathParam("cid") int cid) {
		Association util=gestionUsersRemote.findAssociationByCid(cid);
		gestionUsersRemote.updateAssociation(util);
		return Response.status(Status.ACCEPTED).entity("donateur modifier avec success !!").build();
	}

	@DELETE
	@Path("/association/{cid}")
	public void deleteAssociation(@PathParam("cid") int cid) {
		Association util=gestionUsersRemote.findAssociationByCid(cid);
		gestionUsersRemote.deleteAssociation(util);
	}
	
	

}








