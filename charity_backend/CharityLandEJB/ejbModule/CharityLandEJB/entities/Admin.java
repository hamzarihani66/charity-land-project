package CharityLandEJB.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Admin extends Utilisateur implements Serializable{
	private String pseudo;
	
	public Admin() {
		super();
	}

	public String getPseudo() {
		return pseudo;
	}
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	
	public Admin(int cid, String mail, String password, String role,String phone,Photo photoId, String pseudo) {
		
		this.pseudo = pseudo;
	}

	@Override
	public String toString() {
		return "Admin ["+super.toString()+"pseudo=" + pseudo + "]";
	}

	

}
