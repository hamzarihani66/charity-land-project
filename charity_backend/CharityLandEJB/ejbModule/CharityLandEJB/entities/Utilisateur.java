package CharityLandEJB.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Utilisateur implements Serializable{
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private int cid;
	private String mail;
	private String password;
	private String role;
	private String phone;
	private String pseudo;
	private String titre;
	private String adresse;
	private String description;
	private String papierVerifier;
	private Boolean estverifier;
	private int cin;
	private String nom;
	private String prenom;
	@Temporal(TemporalType.DATE)
	private Date dateNaissance;
	private String gouvernorat;

	private String photoId;
	
	public Utilisateur() {
		super();
	}
	
	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPapierVerifier() {
		return papierVerifier;
	}

	public void setPapierVerifier(String papierVerifier) {
		this.papierVerifier = papierVerifier;
	}

	public Boolean getEstverifier() {
		return estverifier;
	}

	public void setEstverifier(Boolean estverifier) {
		this.estverifier = estverifier;
	}

	public int getCin() {
		return cin;
	}

	public void setCin(int cin) {
		this.cin = cin;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getGouvernorat() {
		return gouvernorat;
	}

	public void setGouvernorat(String gouvernorat) {
		this.gouvernorat = gouvernorat;
	}

	public String getPhotoId() {
		return photoId;
	}

	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}

	

//==========Admin
	public Utilisateur(int cid, String mail, String password, String role, String phone, String pseudo, String photoId) {
		super();
		this.cid = cid;
		this.mail = mail;
		this.password = password;
		this.role = role;
		this.phone = phone;
		this.pseudo = pseudo;
		this.photoId = photoId;
	}
//==========Association
	public Utilisateur(int cid, String mail, String password, String role, String phone, String titre, String adresse,
			String description, String papierVerifier, Boolean estverifier, String photoId) {
		super();
		this.cid = cid;
		this.mail = mail;
		this.password = password;
		this.role = role;
		this.phone = phone;
		this.titre = titre;
		this.adresse = adresse;
		this.description = description;
		this.papierVerifier = papierVerifier;
		this.estverifier = estverifier;
		this.photoId = photoId;
	}

	
//donateur
	public Utilisateur(int cid, String mail, String password, String role, String phone, String adresse, int cin,
			String nom, String prenom, Date dateNaissance, String gouvernorat, String photoId) {
		super();
		this.cid = cid;
		this.mail = mail;
		this.password = password;
		this.role = role;
		this.phone = phone;
		this.adresse = adresse;
		this.cin = cin;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.gouvernorat = gouvernorat;
		this.photoId = photoId;
	}

	@Override
	public String toString() {
		return "Utilisateur [cid=" + cid + ", mail=" + mail + ", password=" + password + ", role=" + role + ", phone="
				+ phone + ", pseudo=" + pseudo + ", titre=" + titre + ", adresse=" + adresse + ", description="
				+ description + ", papierVerifier=" + papierVerifier + ", estverifier=" + estverifier + ", cin=" + cin
				+ ", nom=" + nom + ", prenom=" + prenom + ", dateNaissance=" + dateNaissance + ", gouvernorat="
				+ gouvernorat + ", photoId=" + photoId + "]";
	}

	

	

	

}
