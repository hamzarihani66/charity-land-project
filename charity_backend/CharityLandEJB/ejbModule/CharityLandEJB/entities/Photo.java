package CharityLandEJB.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Photo implements Serializable{
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private int id;
	private String url;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	public Photo() {
		super();
	}
	public Photo(int id, String url) {
		super();
		this.id = id;
		this.url = url;
	}
	@Override
	public String toString() {
		return "Photo [id=" + id + ", url=" + url + "]";
	}
	

}
