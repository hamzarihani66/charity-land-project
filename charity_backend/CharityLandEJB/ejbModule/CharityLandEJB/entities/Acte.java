package CharityLandEJB.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Acte implements Serializable{
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private int id;
	private String titre;
	private String description;
	private boolean estlivre;
	
	private String photosUrl;
	
	
	
	private int associationId;
	
	
	
	public int getAssociationId() {
		return associationId;
	}

	public void setAssociationId(int associationId) {
		this.associationId = associationId;
	}

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isEstlivre() {
		return estlivre;
	}
	public void setEstlivre(boolean estlivre) {
		this.estlivre = estlivre;
	}
	public Acte() {
		super();
	}
	

	public Acte(int id, String titre, String description, boolean estlivre,
			int associationId) {
		super();
		this.id = id;
		this.titre = titre;
		this.description = description;
		this.estlivre = estlivre;
		this.associationId = associationId;
	}

	@Override
	public String toString() {
		return "Acte [id=" + id + ", titre=" + titre + ", description=" + description + ", estlivre=" + estlivre
				+ ", rubriqueId=" + ", associationId=" + associationId + "]";
	}

	
	

}
