package CharityLandEJB.services;

import java.util.List;

import javax.ejb.Local;

import CharityLandEJB.entities.Admin;
import CharityLandEJB.entities.Association;
import CharityLandEJB.entities.Donateur;

@Local
public interface GestionUsersLocal {

	void ajouterDonateur(Donateur don);

	void updateDonateur(Donateur don);

	Donateur findDonateurByCid(int c);

	void deleteDonateur(Donateur don);

	List<Donateur> findAllDonateur();
	
	Donateur LoginDonateur(String mail, String password);
// remote association
	
	List<Association> findAllAssociation();

	void ajouterAssociation(Association asso);

	void updateAssociation(Association asso);

	Association findAssociationByCid(int c);

	void deleteAssociation(Association asso);
	
	Association LoginAssociation(String mail, String password);
	// remote admin

	void ajouterAdmin(Admin admin);

	void updateAdmin(Admin admin);

	Admin findAdminByCid(int c);

	void deleteAdmin(Admin admin);

	List<Admin> findAllAdmin();
	
	Admin LoginAdmin(String mail, String password);
	

}
