package CharityLandEJB.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import CharityLandEJB.entities.Acte;
import CharityLandEJB.entities.Utilisateur;

@Stateless
@LocalBean
public class GestionActes implements GestionActesRemote, GestionActesLocal {
	@PersistenceContext(unitName = "CharityLandEJB")
	EntityManager em;
	@Override
	public void ajouterActe(Acte acte) {
		em.persist(acte);
	}

	@Override
	public void updateActe(Acte acte) {
		em.merge(acte);
	}

	@Override
	public Utilisateur findActeByCid(int id) {
		return (em.find(Utilisateur.class, id));
	}

	@Override
	public void deleteActe(Acte acte) {
		em.remove(em.merge(acte));
	}

	@Override
	public List<Acte> findAllActes() {
		Query q = em.createQuery("From Acte a", Acte.class);
		return (q.getResultList());
	}


    public GestionActes() {
        // TODO Auto-generated constructor stub
    }

}
