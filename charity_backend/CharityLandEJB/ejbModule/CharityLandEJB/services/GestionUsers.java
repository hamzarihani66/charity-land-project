package CharityLandEJB.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import CharityLandEJB.entities.Admin;
import CharityLandEJB.entities.Association;
import CharityLandEJB.entities.Donateur;
import CharityLandEJB.entities.Utilisateur;

@Stateless
@LocalBean
public class GestionUsers implements GestionUsersRemote, GestionUsersLocal {
	@PersistenceContext(unitName = "CharityLandEJB")
	EntityManager em;

	//===============================les api des utilisateurs
	@Override
	public void ajouterUser(Utilisateur user) {
		em.persist(user);
	}

	@Override
	public void updateUser(Utilisateur user) {
		em.merge(user);
	}

	@Override
	public Utilisateur findUserByCid(int c) {
		return (em.find(Utilisateur.class, c));
	}

	@Override
	public void deleteUser(Utilisateur user) {
		em.remove(em.merge(user));
	}

	@Override
	public List<Utilisateur> findAllUsers() {
		Query q = em.createQuery("From Utilisateur u", Utilisateur.class);
		return (q.getResultList());
	}

	public Utilisateur LoginUser(String mail, String password) {
		Utilisateur found = null;
		try {
			String jpql = "from Utilisateur u where u.mail=:param1 and u.password=:param2";
			Query qu = em.createQuery(jpql);
			qu.setParameter("param1", mail);
			qu.setParameter("param2", password);
			found = (Utilisateur) qu.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return found;
	}
	//===============================les api des donateurs
	@Override
	public void ajouterDonateur(Donateur don) {
		em.persist(don);
	}

	@Override
	public void updateDonateur(Donateur don) {
		em.merge(don);
	}

	@Override
	public Donateur findDonateurByCid(int c) {
		return (em.find(Donateur.class, c));
	}

	@Override
	public void deleteDonateur(Donateur don) {
		em.remove(em.merge(don));
	}

	@Override
	public List<Donateur> findAllDonateur() {
		Query q = em.createQuery("From Donateur d", Donateur.class);
		return (q.getResultList());
	}

	public Donateur LoginDonateur(String mail, String password) {
		Donateur found = null;
		try {
			String jpql = "from Donateur d where d.mail=:param1 and d.password=:param2";
			Query qu = em.createQuery(jpql);
			qu.setParameter("param1", mail);
			qu.setParameter("param2", password);
			found = (Donateur) qu.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return found;
	}

	// methode association
	@Override
	public void ajouterAssociation(Association asso) {
		em.persist(asso);
	}

	@Override
	public void updateAssociation(Association asso) {
		em.merge(asso);
	}

	@Override
	public Association findAssociationByCid(int c) {
		return (em.find(Association.class, c));
	}

	@Override
	public void deleteAssociation(Association asso) {
		em.remove(em.merge(asso));
	}

	@Override
	public List<Association> findAllAssociation() {
		Query q = em.createQuery("From Association a", Association.class);
		return (q.getResultList());
	}

	public Association LoginAssociation(String mail, String password) {
		Association found = null;
		try {
			String jpql = "from Association a where a.mail=:param1 and a.password=:param2";
			Query qu = em.createQuery(jpql);
			qu.setParameter("param1", mail);
			qu.setParameter("param2", password);
			found = (Association) qu.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return found;
	}

	// Methode Admin
	@Override
	public void ajouterAdmin(Admin admin) {
		em.persist(admin);
	}

	@Override
	public void updateAdmin(Admin admin) {
		em.merge(admin);
	}

	@Override
	public Admin findAdminByCid(int c) {
		return (em.find(Admin.class, c));
	}

	@Override
	public void deleteAdmin(Admin admin) {
		em.remove(em.merge(admin));
	}

	@Override
	public List<Admin> findAllAdmin() {
		Query q = em.createQuery("From Admin a", Admin.class);
		return (q.getResultList());
	}

	public Admin LoginAdmin(String mail, String password) {
		Admin found = null;
		try {
			String jpql = "from Admin a where a.mail=:param1 and a.password=:param2";
			Query qu = em.createQuery(jpql);
			qu.setParameter("param1", mail);
			qu.setParameter("param2", password);
			found = (Admin) qu.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return found;
	}

	public GestionUsers() {
		// TODO Auto-generated constructor stub
	}

}
