import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  mail: any
  password: any
  loading: any = false;

  constructor(private authService : AuthService) { }

  ngOnInit(): void {
  }
  sumbit(){

    this.loading = true
    this.loading = this.authService.login(this.mail,this.password)
  }

}
