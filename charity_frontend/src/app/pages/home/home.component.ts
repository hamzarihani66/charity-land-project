import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'Home';
  navSidebarClass: boolean = true;
  hamburgerClass: boolean = false;

    constructor(public sharedService: SharedService) {
      
    }

  ngOnInit(): void {
  }

}
