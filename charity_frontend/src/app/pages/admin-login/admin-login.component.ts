
import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {

  mail: any
  password: any
  loading: any = false;

  constructor(private authService : AuthService) { }

  ngOnInit(): void {
  }

  sumbit(){
    this.loading = true
    this.loading = this.authService.login(this.mail,this.password)
  }
}
