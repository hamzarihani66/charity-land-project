import { WebRequestService } from 'src/app/services/web-request.service';
import { Component, Input, OnInit } from '@angular/core';
import { AdminUpdateDialogService } from 'src/app/services/admin-update-dialog.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ass-update',
  templateUrl: './ass-update.component.html',
  styleUrls: ['./ass-update.component.css']
})
export class AssUpdateComponent implements OnInit {

  data: any;
  subscription: Subscription = new Subscription();
  selecetdFile: String = 'choisir un fichier';
  imageError: string = '';
  isImageSaved: boolean = false;
  cardImageBase64: string = '';
  files: File[] = [];
  image64: any;
  Etat: any[] = [
    { name: 'Verifier', value: true },
    { name: 'Non verifier', value: false },
  ];

  constructor(
    private updataData: AdminUpdateDialogService,
    private webRequest: WebRequestService,
    private dialogRef: MatDialog
  ) {}

  ngOnInit(): void {
    this.subscription = this.updataData.currentData.subscribe((data: any) => {
      this.data = data
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  save() {
    Swal.fire({
      title: 'Êtes-vous sûr de vouloir mettre à jour cette acte?',
      showDenyButton: true,
      confirmButtonText: 'Oui',
      denyButtonText: `Annuler`,
    }).then((result) => {
      if (result.isConfirmed) {
        if (this.image64) this.data.photoUrl = this.image64;
        console.log(this.data);
        this.webRequest.put('users/' + this.data.cid, this.data).subscribe(
          (ele) => {},
          (error) => {
            this.dialogRef.closeAll();
            Swal.fire('Mise à jour réussie', '', 'success');
          }
        );
      }
    });
  }

  onSelect(event: any) {
    let self = this;
    this.files = [];
    this.files.push(event.addedFiles[0]);
    let reader = new FileReader();
    reader.readAsDataURL(this.files[0]);
    reader.onload = function () {
      self.image64 = reader.result;
    };
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

}
