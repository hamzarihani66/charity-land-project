import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssUpdateComponent } from './ass-update.component';

describe('AssUpdateComponent', () => {
  let component: AssUpdateComponent;
  let fixture: ComponentFixture<AssUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
