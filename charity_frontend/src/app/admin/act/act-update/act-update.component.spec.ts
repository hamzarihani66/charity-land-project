import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActUpdateComponent } from './act-update.component';

describe('ActUpdateComponent', () => {
  let component: ActUpdateComponent;
  let fixture: ComponentFixture<ActUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
