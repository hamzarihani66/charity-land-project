import { DonUpdateComponent } from './don-update/don-update.component';
import { AssUpdateComponent } from './../association/ass-update/ass-update.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/models/user';
import { AdminUpdateDialogService } from 'src/app/services/admin-update-dialog.service';
import { WebRequestService } from 'src/app/services/web-request.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-donor',
  templateUrl: './donor.component.html',
  styleUrls: ['./donor.component.css']
})
export class DonorComponent implements OnInit {
  donateurs = new Array<User>();

  constructor(private webRequest: WebRequestService, private dialog: MatDialog,private updataData: AdminUpdateDialogService) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.webRequest.get('donateurs').subscribe((donateurs: any) => {
      this.donateurs = donateurs;
    });
  }
  checkUncheckAll(event: any) {
    var checkboxes = document.getElementsByTagName('input');
    if (event.target.checked) {
      for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].type == 'checkbox') {
          checkboxes[i].checked = true;
        }
      }
    } else {
      for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].type == 'checkbox') {
          checkboxes[i].checked = false;
        }
      }
    }
  }
  delete(titre: any, id: any) {
    console.log(id);
    Swal.fire({
      title: 'Êtes-vous sûr de vouloir supprimer cet acte?',
      showDenyButton: true,
      confirmButtonText: 'Supprimer',
      denyButtonText: `Annuler`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.webRequest.delete('users/' + id.toString()).subscribe(
          (elm) => {},
          (error) => {
            this.loadData();
            Swal.fire(titre + ' a été bien supprimé!', '', 'success');
          }
        );
      }
    });
  }

  update(data: any){
    this.updataData.changeData(data)
    this.dialog.open(DonUpdateComponent)
  }
}
