import { WebRequestService } from 'src/app/services/web-request.service';
import { Component, Input, OnInit } from '@angular/core';
import { AdminUpdateDialogService } from 'src/app/services/admin-update-dialog.service';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-voir',
  templateUrl: './voir.component.html',
  styleUrls: ['./voir.component.css']
})
export class VoirComponent implements OnInit {

  data: any;
  ass: any;
  subscription: Subscription = new Subscription();
  associations = new Array<any>();


  constructor(
    private updataData: AdminUpdateDialogService,
    private webRequest: WebRequestService,
    private dialogRef: MatDialog
  ) {}

  ngOnInit(): void {
    this.subscription = this.updataData.currentData.subscribe((data: any) => {
      this.data = data.data;

      this.ass = data.ass;
      console.log(this.ass)
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    
  }

  close(){
    this.dialogRef.closeAll()
  }
}
