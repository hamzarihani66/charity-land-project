import { VoirComponent } from './voir/voir.component';
import { WebRequestService } from './../../services/web-request.service';
import { Component, OnInit } from '@angular/core';

import { Act } from 'src/app/models/act';
import { User } from 'src/app/models/user';
import { MatDialog } from '@angular/material/dialog';
import { AdminUpdateDialogService } from 'src/app/services/admin-update-dialog.service';

@Component({
  selector: 'app-rubriques',
  templateUrl: './rubriques.component.html',
  styleUrls: ['./rubriques.component.css'],
})
export class RubriquesComponent implements OnInit {

  actes = new Array<Act>();
  associations = new Array<User>();

  constructor(private webRequest: WebRequestService, private dialog: MatDialog,private updataData: AdminUpdateDialogService) {}
  ngOnInit(): void {
    this.loadData();
  }

  getAss(id: any) {
    return this.associations.find((element: User) => element.cid == id);
  }
  loadData() {
    console.log();
    this.webRequest.get('associations').subscribe((associations: any) => {
      this.associations = associations;

      this.webRequest.get('actes').subscribe((actes: any) => {

        this.actes = actes;
        this.actes = this.actes.filter((act) => act.estlivre === false);

      });
    });
  }



  see(data: any){
    this.updataData.changeData({data:data,ass:this.getAss(data.associationId)})
    this.dialog.open(VoirComponent)
  }
}
