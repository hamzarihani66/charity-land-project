import { Router } from '@angular/router';
import { WebRequestService } from './../../services/web-request.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-signindonnateur',
  templateUrl: './signindonnateur.component.html',
  styleUrls: ['./signindonnateur.component.css']
})
export class SignindonnateurComponent implements OnInit {

  files: File[] = [];

  ngOnInit(): void {
  }

  angForm: FormGroup | any;

  constructor(private fb: FormBuilder,private modalService: NgbModal,private webRequest:WebRequestService,private router:Router) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       cin: ['', [Validators.required, Validators.minLength(8),Validators.maxLength(8) ]],
       mail: ['', [Validators.required ,  Validators.email, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
       password: ['', [Validators.required, Validators.minLength(8),,Validators.maxLength(8)] ],
       pseudo: ['', [Validators.required]],
       phone: ['', [Validators.required,Validators.minLength(8),Validators.maxLength(8)]],
       dateNaissance:['', [Validators.required]],
       gouvernorat: ['', [Validators.required]],
       adresse: ['', [Validators.required]],
       role: ['donateur'],
       photoUrl: ['assets/images/download (1).jfif'],

    });
  }


  toggleEye: boolean = true;

  toggleEyeIcon(inputPassword:any) {
		this.toggleEye = !this.toggleEye;

		inputPassword.type = inputPassword.type === 'password' ? 'text' : 'password';


	}
	open(content:any) {
		this.modalService.open(content);
	}



  onSelect(event: any) {
    let self = this;
    this.files = [];
    this.files.push(event.addedFiles[0]);
    let reader = new FileReader();
    reader.readAsDataURL(this.files[0]);
    reader.onload = function () {
      self.angForm.get('photoUrl').setValue(reader.result);
    };
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }


  submit() {

    this.angForm.get('dateNaissance').setValue(this.angForm.get('dateNaissance').value+'T00:00:00')
    console.log(this.angForm.value)
    this.webRequest.post('users/add', this.angForm.value).subscribe(
      (elm) => {},
      (error) => {
        Swal.fire(
          'Succès!',
          'Votre compte a été créé avec succès',
          'success'
        )
        this.router.navigate(['login']);
      }
    );
  }
}
