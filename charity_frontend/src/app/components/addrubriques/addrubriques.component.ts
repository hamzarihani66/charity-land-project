import { WebRequestService } from './../../services/web-request.service';
import { Component, OnInit } from '@angular/core';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addrubriques',
  templateUrl: './addrubriques.component.html',
  styleUrls: ['./addrubriques.component.css'],
})
export class AddrubriquesComponent implements OnInit {
  files: File[] = [];
  user = new User();
  angForm: FormGroup | any;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private webRequest: WebRequestService
  ) {}

  ngOnInit(): void {
    this.user = this.authService.currentUserValue;
    this.angForm = this.fb.group({
      titre: ['', [Validators.required]],
      description: ['', [Validators.required]],
      estlivre: [false],
      photosUrl: [''],
      associationId: [this.user.cid],
    });
  }
  onSelect(event: any) {
    let self = this;
    this.files = [];
    this.files.push(event.addedFiles[0]);
    let reader = new FileReader();
    reader.readAsDataURL(this.files[0]);
    reader.onload = function () {
      self.angForm.get('photosUrl').setValue(reader.result);
    };
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  submit() {
    this.webRequest.post('actes', this.angForm.value).subscribe(
      (elem) => {},
      (error) => {
        Swal.fire(
          'Succès',
          'Votre rubrique a été ajouté avec succès',
          'success'
        )
        this.angForm.reset()
        this.files = []
      }
    );
  }
}
