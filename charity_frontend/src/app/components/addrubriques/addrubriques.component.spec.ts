import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddrubriquesComponent } from './addrubriques.component';

describe('AddrubriquesComponent', () => {
  let component: AddrubriquesComponent;
  let fixture: ComponentFixture<AddrubriquesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddrubriquesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddrubriquesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
