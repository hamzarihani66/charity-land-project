import { ActivatedRoute, Router } from '@angular/router';
import { WebRequestService } from './../../services/web-request.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  user = new User();
  id: String = '';
  constructor(
    private modalService: NgbModal,
    private fb: FormBuilder,
    private webRequest: WebRequestService,
    private router: ActivatedRoute
  ) {
    this.createForm();
  }

  ngOnInit(): void {
    this.id = this.router.snapshot.params['id'];
    this.webRequest.get('users/' + this.id).subscribe((elm) => {
      this.user = elm;
      console.log(this.user)
    });
  }
  news = [
    {
      image: 'assets/images/profile/5.jpg',
      title: 'Collection of textile samples',
      description:
        'I shared this on my fb wall a few months back, and I thought.',
      url: 'admin/post-details',
    },
  ];

  open(content: any): void {
    this.modalService.open(content);
  }

  angForm: FormGroup | any;

  createForm() {
    this.angForm = this.fb.group({
      cin: [
        '',
        [Validators.required, Validators.minLength(8), Validators.maxLength(8)],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],
      motdepasse: [
        '',
        [
          Validators.required,
          Validators.minLength(8),
          ,
          Validators.maxLength(8),
        ],
      ],
      nometprenom: ['', [Validators.required]],
      tel: [
        '',
        [Validators.required, Validators.minLength(8), Validators.maxLength(8)],
      ],
      datedenaissance: ['', [Validators.required]],
      gouvernorat: ['', [Validators.required]],
      photo: ['', [Validators.required]],
      adresse: ['', [Validators.required]],
    });
  }

  toggleEye: boolean = true;

  toggleEyeIcon(inputPassword: any) {
    this.toggleEye = !this.toggleEye;

    inputPassword.type =
      inputPassword.type === 'password' ? 'text' : 'password';
  }
}
