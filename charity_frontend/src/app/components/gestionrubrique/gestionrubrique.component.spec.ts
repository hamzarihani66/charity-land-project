import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionrubriqueComponent } from './gestionrubrique.component';

describe('GestionrubriqueComponent', () => {
  let component: GestionrubriqueComponent;
  let fixture: ComponentFixture<GestionrubriqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionrubriqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionrubriqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
