import { AuthService } from 'src/app/services/auth.service';
import { UpdaterubriqueComponent } from './updaterubrique/updaterubrique.component';
import { AdminUpdateDialogService } from './../../services/admin-update-dialog.service';
import { User } from 'src/app/models/user';
import { Component, OnInit, ViewChild } from '@angular/core';
import { WebRequestService } from 'src/app/services/web-request.service';
import { Act } from 'src/app/models/act';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-gestionrubrique',
  templateUrl: './gestionrubrique.component.html',
  styleUrls: ['./gestionrubrique.component.css'],
})
export class GestionrubriqueComponent implements OnInit {
  subscription: Subscription = new Subscription();

  actes = new Array<Act>();

  constructor(
    private webRequest: WebRequestService,
    private dialog: MatDialog,
    private updataData: AdminUpdateDialogService,
    private auth:AuthService
  ) {}

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.actes = []
    this.webRequest.get('actes').subscribe((actes: any) => {
      this.actes = actes;
      this.actes = this.actes.filter((act) => act.associationId === this.auth.currentUserValue.cid);
    });
  }
  checkUncheckAll(event: any) {
    var checkboxes = document.getElementsByTagName('input');
    if (event.target.checked) {
      for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].type == 'checkbox') {
          checkboxes[i].checked = true;
        }
      }
    } else {
      for (var i = 0; i < checkboxes.length; i++) {
        // console.log(i)
        if (checkboxes[i].type == 'checkbox') {
          checkboxes[i].checked = false;
        }
      }
    }
  }

  delete(titre: any, id: any) {
    console.log(id);
    Swal.fire({
      title: 'Êtes-vous sûr de vouloir supprimer cet acte?',
      showDenyButton: true,
      confirmButtonText: 'Supprimer',
      denyButtonText: `Annuler`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.webRequest.delete('actes/' + id.toString()).subscribe(
          (elm) => {},
          (error) => {
            this.loadData();
            Swal.fire(titre + ' a été bien supprimé!', '', 'success');
          }
        );
      }
    });
  }

  update(data: any) {
    this.updataData.changeData({ data: data });
    this.dialog.open(UpdaterubriqueComponent);
  }
}
