import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdaterubriqueComponent } from './updaterubrique.component';

describe('UpdaterubriqueComponent', () => {
  let component: UpdaterubriqueComponent;
  let fixture: ComponentFixture<UpdaterubriqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdaterubriqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdaterubriqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
