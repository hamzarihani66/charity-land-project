
import { Component, OnInit, ViewChild } from '@angular/core';
import { WebRequestService } from 'src/app/services/web-request.service';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { AdminUpdateDialogService } from 'src/app/services/admin-update-dialog.service';

@Component({
  selector: 'app-updaterubrique',
  templateUrl: './updaterubrique.component.html',
  styleUrls: ['./updaterubrique.component.css']
})
export class UpdaterubriqueComponent implements OnInit {
  data: any;
  subscription: Subscription = new Subscription();
  selecetdFile: String = 'choisir un fichier';
  imageError: string = '';
  isImageSaved: boolean = false;
  cardImageBase64: string = '';
  files: File[] = [];
  image64: any;
  Etat: any[] = [
    { name: 'Livré', value: true },
    { name: 'En ligne', value: false },
  ];

  constructor(
    private updataData: AdminUpdateDialogService,
    private webRequest: WebRequestService,
    private dialogRef: MatDialog
  ) {}

  ngOnInit(): void {
    this.subscription = this.updataData.currentData.subscribe((data: any) => {
      this.data = data.data;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  save() {
    Swal.fire({
      title: 'Êtes-vous sûr de vouloir mettre à jour cette acte?',
      showDenyButton: true,
      confirmButtonText: 'Oui',
      denyButtonText: `Annuler`,
    }).then((result) => {
      if (result.isConfirmed) {
        if (this.image64) this.data.photosUrl = this.image64;
        console.log(this.data);
        this.webRequest.put('actes/' + this.data.id, this.data).subscribe(
          (ele) => {},
          (error) => {
            this.dialogRef.closeAll();
            Swal.fire('Mise à jour réussie', '', 'success');
          }
        );
      }
    });
  }

  onSelect(event: any) {
    let self = this;
    this.files = [];
    this.files.push(event.addedFiles[0]);
    let reader = new FileReader();
    reader.readAsDataURL(this.files[0]);
    reader.onload = function () {
      self.image64 = reader.result;
    };
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }
}
