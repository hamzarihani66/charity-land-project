export class Act {
  id?: number;
  titre?: String;
  description?: String;
  photosUrl?: Blob;
  estlivre?: Boolean;
  associationId?: number;
  key: any;
  donatuersId: Array<String> = [];
}
