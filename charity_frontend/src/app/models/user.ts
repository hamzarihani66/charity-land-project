export class User {
  cid?: number;
  pseudo?: String;
  mail?: String;
  password?: String;
  role?: String;
  phone?: String;
  titre?: String;
  adresse?: String;
  description?: String;
  papierVerifier?: String;
  estverifier?: String;
  cin?: number;
  nom?: String;
  prenom?: String;
  dateNaissance?: Date;
  gouvernorat?: String;
  photoUrl?: Blob;
  authdata?: string;
}
