import { DonGuard } from './guards/don-auth.guard';
import { AssGuard } from './guards/ass-auth.guard';
import { ProfileComponent } from './components/profile/profile.component';
import { GestionrubriqueComponent } from './components/gestionrubrique/gestionrubrique.component';
import { AddrubriquesComponent } from './components/addrubriques/addrubriques.component';

import { RubriquesComponent } from './components/rubriques/rubriques.component';
import { HomeComponent } from './pages/home/home.component';
import { AdminLoginComponent } from './pages/admin-login/admin-login.component';
import { ActComponent } from './admin/act/act.component';
import { AssociationComponent } from './admin/association/association.component';
import { DonorComponent } from './admin/donor/donor.component';
import { UserComponent } from './admin/user/user.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin/admin.component';

import { AdminAuthGuard } from './guards/admin-auth.guard';
import { Error404Component } from './pages/error404/error404.component';
import { LoginComponent } from './pages/login/login.component';
import { FormaddComponent } from './pages/formadd/formadd.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home/admin', redirectTo: 'admin', pathMatch: 'full' },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      { path: '', component: DonorComponent },
      { path: 'user-list', component: UserComponent },
      { path: 'donor-list', component: DonorComponent },
      { path: 'associaion-list', component: AssociationComponent },
      { path: 'act-list', component: ActComponent },
    ],
    canActivate: [AdminAuthGuard],
  },
  {path: 'login',component: LoginComponent},
  {path: 'signin',component: FormaddComponent},
  {
    path: 'home',
    component: HomeComponent,
    children: [
      { path: '', redirectTo: 'rubrique', pathMatch: 'full'},
      { path: 'rubrique', component: RubriquesComponent },
      { path: 'ajouterubrique', component: AddrubriquesComponent,canActivate: [AssGuard] },
      { path: 'gestionrubrique', component: GestionrubriqueComponent,canActivate: [AssGuard] },
      { path: 'profile/:id', component: ProfileComponent }
    ],
/*     canActivate: [], */
  },

  { path: 'admin/login', component: AdminLoginComponent },

  { path: '**', component: Error404Component },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
