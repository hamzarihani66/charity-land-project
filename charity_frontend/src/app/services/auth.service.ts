import { User } from './../models/user';
import { WebRequestService } from './web-request.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private webRequestService: WebRequestService,
    private router: Router
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('user') || '{}')
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(mail: string, password: string) {
    this.webRequestService.login(mail, password).subscribe(
      (user: User) => {
        user.authdata = window.btoa(mail + ':' + password);
        localStorage.setItem('user', JSON.stringify(user));
        this.currentUserSubject.next(user);
        if (user.role == "admin")
        this.router.navigate(['admin/donor-list']);
        else
        this.router.navigate(['./']);
        Swal.fire(
          'Connexion Réussie',
          'Vous pouvez fermer cette fenêtre et continuer à utiliser le site Web',
          'success'
        )
      },
      (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Mot de passe ou email incorrect !',
        })
        return false
      }
    );
  }

  logout() {
    localStorage.removeItem('user');
    this.currentUserSubject.next({});
    this.router.navigate(['login']);

  }
}
